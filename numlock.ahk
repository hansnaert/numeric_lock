#Include, %A_ScriptDir%\libraries\tooltip.ahk

Menu, Tray, Tip, Num Lock
_ChangeAppearance() {
    if (GetKeyState("NumLock", "T")) {
        Menu, Tray, Icon, icons/numlock_on.png
    }
    else {
        Menu, Tray, Icon, icons/numlock_off.png
    }
}

_ChangeAppearance()

_ShowTooltip(message:="") {
    params := {}
    params.message := message
    params.lifespan := TooltipsLifespan
    params.position := TooltipsCentered
    params.fontSize := TooltipsFontSize
    params.fontWeight := TooltipsFontInBold
    params.fontColor := TooltipsFontColor
    params.backgroundColor := TooltipsBackgroundColor
    Toast(params)
}

_change()
{
;msg := "Caps Lock: " (GetKeyState("CapsLock", "T") ? "ON" : "OFF") "`n"
msg := "Num Lock: " (GetKeyState("NumLock", "T") ? "ON" : "OFF")
;msg := msg "Scroll Lock: " (GetKeyState("ScrollLock", "T") ? "ON" : "OFF")
 ;Sleep, 10	; drastically improves reliability on slower systems (took a loooong time to figure this out)
_ChangeAppearance()
_ShowTooltip(msg)
}

_change()

;=============================================================================================
; Show a ToolTip that shows the current state of the lock keys (e.g. CapsLock) when one is pressed
;=============================================================================================
~*NumLock::
;~*CapsLock::
;~*ScrollLock::
_change()
return
